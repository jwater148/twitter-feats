// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/mark/Documents/play-java-api/conf/routes
// @DATE:Tue Mar 09 11:13:45 GMT 2021


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
